import React, { Component } from 'react';
import './App.css';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      validate: true,
      newTodo: '',
      todoList: [
        {
          id: 1,
          todo: "To do 1",
          done: true
        },
        {
          id: 2,
          todo: "To do 2",
          done: false
        },
        {
          id: 3,
          todo: "To do 3",
          done: true
        }
      ]
    }
  }
  handleToggle = (id) => {
    let todoList = [...this.state.todoList]
    todoList = todoList.map(elem => {
      if (elem.id === id) elem.done = !elem.done
      return elem
    })
    this.setState({todoList})
  }
  handleNewTodo = () => {
    if (this.state.newTodo === '') {
      this.setState({validate: false})
    } else {
      let todoList= [...this.state.todoList]
      let random = Math.random() * 80 + this.state.newTodo
      todoList.push({
        id: random,
        todo: this.state.newTodo,
        done: false
      })
      this.setState({todoList, newTodo: ''})
    }
  }
  render() {
    return (
      <div className="App">
        <h1>My TodoList</h1>
        <List>
        {
          this.state.todoList.map(value => (
          <ListItem key={value.id} role={undefined} dense button onClick={() => this.handleToggle(value.id)}>
            <Checkbox
              checked={value.done}
              tabIndex={-1}
              disableRipple
            />
            <ListItemText primary={value.todo} className={value.done ? 'active' : null} />
          </ListItem>
        ))}
        </List>
        <div className="newTodos">
          <Input placeholder="Add Todos" value={this.state.newTodo} error={!this.state.validate} onChange={(e) => {this.setState({newTodo: e.target.value, validate: true})}} />
          <Button variant="contained" color="primary" onClick={() => {this.handleNewTodo()}}>Add</Button>
        </div>
        <p className="done">Done &nbsp;
        {this.state.todoList.reduce((total, elem) => {
          if (elem.done) total = total + 1;
          return total;
        }, 0)} / {this.state.todoList.length}</p>
      </div>
    );
  }
}

export default App;
